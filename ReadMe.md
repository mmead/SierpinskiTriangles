# SierpinskiTriangles

<p align="center">
  <img src="/doc/screenshotColor.png" alt="Sierpinski Triangle"/>
</p>  

Creates Sierpinski Triangles using various algorithm implementations.

## Current implementations

* [Chaotic method](https://en.wikipedia.org/wiki/Sierpinski_triangle#Chaos_game)

## External dependencies

* [OpenCV](http://opencv.org/)  
The application uses OpenCV for handling the image processing operations.
OpenCV is a cross platform image processing library and is readily
available via most package managers. The version used for development is
3.1.0. However, the components used are minimal; you can likely get away
with a less recent version.

## Compiling, running, and testing the code

This application uses [CMake](https://cmake.org/) for cross platform builds, and
was tested using version 3.8.0 on Fedora 25. It uses CMake's `ctest` for unit
testing.

To save a few keystrokes, you can use the automated build and run shell scripts
located under `util`. Please note that the build scripts are expected to be run
from the project root directory.

### Build

Builds the code under the project root directory in a directory called `build`.
Installs the executable under the project root directory in a directory called `install`.

```bash
$ cd SierpinskiTriangles
$ sh util/Build.sh
```

### Run

Runs the executable under `install/bin`.

```bash
$ cd SierpinskiTriangles
$ sh util/Run.sh
```

### Test

Runs the unit tests.

```bash
$ cd SierpinskiTriangles
$ sh util/RunTests.sh
```

## License

Copyright © Michael Mead, since 2017.

Distributed under the MIT license. Please see the LICENSE file for details.

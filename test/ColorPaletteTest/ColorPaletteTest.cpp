//*******************************************************************************
// Author: Michael Mead
//
// Copyright 2017 Michael Mead
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//*******************************************************************************

#include <ColorPalette.h>

#include <iostream>
#include <stdexcept>
#include <string>

//*******************************************************************************
//*******************************************************************************
void RunTest()
{
  ColorPalette colorPalette;

  cv::Vec3b Blue(255, 0, 0);
  cv::Vec3b Green(0, 255, 0);
  cv::Vec3b Red(0, 0, 255);

  colorPalette.AddColor(0, Blue);
  if (colorPalette.GetColor(0) != Blue)
  {
    throw std::runtime_error("Failed to retrieve Blue.");
  }

  colorPalette.AddColor(1, Green);
  if (colorPalette.GetColor(1) != Green)
  {
    throw std::runtime_error("Failed to retrieve Green.");
  }

  colorPalette.AddColor(2, Red);
  if (colorPalette.GetColor(2) != Red)
  {
    throw std::runtime_error("Failed to retrieve Red");
  }

  if (colorPalette.GetColor(3) != cv::Vec3b(0, 0, 0))
  {
    throw std::runtime_error("Failed to retrieve default color.");
  }

  colorPalette.Clear();
  if (colorPalette.GetColor(0) != cv::Vec3b(0, 0, 0))
  {
    throw std::runtime_error("Failed to clear color palette.");
  }
}

//*******************************************************************************
//*******************************************************************************
int main(int argc, char** argv)
{
  std::cout << "Starting ColorPaletteTest..." << std::endl;

  try
  {
    RunTest();
  }
  catch (const std::runtime_error& Error)
  {
    std::cerr << "ERROR: " << Error.what() << std::endl;
    return 1;
  }

  std::cout << "Passed." << std::endl;
  return 0;
}

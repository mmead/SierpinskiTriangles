//*******************************************************************************
// Author: Michael Mead
//
// Copyright 2017 Michael Mead
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//*******************************************************************************

#include <MathTypes.h>

#include <iostream>
#include <stdexcept>
#include <string>

//*******************************************************************************
//*******************************************************************************
void RunTest()
{
  using namespace MathTypes;

  Vertices vertices;

  vertices.data[0] = cv::Point2i(0, 0);
  if (vertices.data[0].x != 0 || vertices.data[0].y != 0)
  {
    throw std::runtime_error("RunTest: First (unsigned) vertex failed test.");
  }

  vertices.data[1] = cv::Point2i(10, 30);
  if (vertices.data[1].x != 10 || vertices.data[1].y != 30)
  {
    throw std::runtime_error("RunTest: Second (unsigned) vertex failed test.");
  }

  vertices.data[2] = cv::Point2i(540, 300);
  if (vertices.data[2].x != 540 || vertices.data[2].y != 300)
  {
    throw std::runtime_error("RunTest: Third (unsigned) vertex failed test.");
  }
}

//*******************************************************************************
//*******************************************************************************
int main(int argc, char** argv)
{
  std::cout << "Starting MathTypesTest..." << std::endl;

  try
  {
    RunTest();
  }
  catch (const std::runtime_error& Error)
  {
    std::cerr << "ERROR: " << Error.what() << std::endl;
    return 1;
  }

  std::cout << "Passed." << std::endl;
  return 0;
}

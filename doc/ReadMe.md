# Project layout

The project is organized in a modular layout. In general, commands and actions
are expected to be run from the project root.

## app/

Contains application level code. This is where the program entry point is located.

## cmake/

Contains cmake files. These are used to find libraries and include directories
for third party packages.

## lib/

Contains common library sources, which at the moment is all C++ source
except for the program entry point.

# test/

Contains the unit tests. The unit tests are built every time the application is
built. This might need to be separated as the amount of code increases, but for
now it provides very quick feedback on compile time errors when refactoring the
code.

## util/

Contains utility scripts that might be useful for developers and people who want
to play around with building and running the code themselves.

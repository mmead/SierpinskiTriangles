//*******************************************************************************
// Author: Michael Mead
//
// Copyright 2017 Michael Mead
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//*******************************************************************************

#include "ChaoticTriangleGeneration.h"

#include <opencv2/core.hpp>

#include <cmath>
#include <random>

//*******************************************************************************
//*******************************************************************************
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
ChaoticTriangleGeneration::ChaoticTriangleGeneration()
  : mColorPalette()
{
  mColorPalette.AddColor(0, cv::Vec3b(105, 111, 255));
  mColorPalette.AddColor(1, cv::Vec3b(92, 204, 255));
  mColorPalette.AddColor(2, cv::Vec3b(176, 216, 136));
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
ChaoticTriangleGeneration::~ChaoticTriangleGeneration()
{
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
ChaoticTriangleGeneration::ChaoticTriangleGeneration(
  const ChaoticTriangleGeneration& Other)
{
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
ChaoticTriangleGeneration& ChaoticTriangleGeneration::operator=(
  const ChaoticTriangleGeneration &Rhs)
{
  return *this;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
cv::Mat ChaoticTriangleGeneration::Compute(const cv::Size2i& triangleSize) const
{
  auto vertices = ConstructVertices(triangleSize);

  std::random_device device;
  std::mt19937 generator(device());
  std::uniform_int_distribution<int> indexDistribution(0, 2);

  cv::Mat image(triangleSize, CV_8UC3, cv::Vec3b(255, 255, 255));
  cv::Point2i pixel(triangleSize.width / 2, triangleSize.height / 2);

  for (int i = 0; i < 1000000; ++i)
  {
    int index = indexDistribution(generator);
    cv::Point2i randomVertex = vertices.data[index];

    pixel = 0.5 * (pixel + randomVertex);
    image.at<cv::Vec3b>(pixel) = mColorPalette.GetColor(index);
  }

  return image;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
MathTypes::Vertices ChaoticTriangleGeneration::ConstructVertices(
  const cv::Size2i& triangleSize) const
{
  MathTypes::Vertices vertices;

  vertices.data[0].x = static_cast<int>(std::ceil(triangleSize.width / 2));
  vertices.data[1].y = 0;

  vertices.data[1].x = 0;
  vertices.data[1].y = triangleSize.height - 1;

  vertices.data[2].x = triangleSize.width - 1;
  vertices.data[2].y = triangleSize.height - 1;

  return vertices;
}

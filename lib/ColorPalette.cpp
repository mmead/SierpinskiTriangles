//*******************************************************************************
// Author: Michael Mead
//
// Copyright 2017 Michael Mead
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//*******************************************************************************

#include "ColorPalette.h"

//*******************************************************************************
//*******************************************************************************
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
ColorPalette::ColorPalette()
  : mDefaultColor(0, 0, 0),
    mColors()
{
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void ColorPalette::AddColor(unsigned index, const cv::Vec3b& color)
{
  mColors.insert(std::make_pair(index, color));
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void ColorPalette::Clear()
{
  mColors.clear();
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const cv::Vec3b& ColorPalette::GetColor(unsigned index) const
{
  auto color = mColors.find(index);

  if (color != mColors.end())
  {
    return color->second;
  }

  return mDefaultColor;
}
